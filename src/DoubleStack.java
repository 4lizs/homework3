package src;

import java.util.LinkedList;

public class DoubleStack {

	private LinkedList<Double> list;

	public static void main(String[] argum) {
	
	}

	public DoubleStack() {
		this.list = new LinkedList<Double>();
		// TODO!!! Your constructor here!
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		
		DoubleStack cloned = new DoubleStack();
		cloned.list.addAll(this.list);
		return cloned;
	}

	public boolean stEmpty() {
		if (list.size() == 0) {
			return true;
		}
		return false; // TODO!!! Your code here!
	}

	public void push(double a) {
		list.push(a);
	}

	public double pop() {
		if (list.isEmpty()) {
			throw new RuntimeException("stack is empty!");
		} else {
			
			return list.removeFirst();// eemaldab stäkist esimese elemendi ja tagastab selle
		}
		// TODO!!! Your code here!
	}

	public void op(String s) {
		if (this.stEmpty()){
			throw new RuntimeException("Magasin on tühi!");
		}
		double element1, element2;
		switch (s.charAt(0)) {
		case '/':
			element2 = this.pop();
			element1 = this.pop();
			push(element1 / element2);
			break;
		case '*':
			element2 = this.pop();
			element1 = this.pop();
			push(element1 * element2);
			break;
		case '+':
			element2 = this.pop();
			element1 = this.pop();
			push(element1 + element2);
			break;
		case '-':
			element2 = this.pop();
			element1 = this.pop();
			push(element1 - element2);
			break;
		default:
			throw new IllegalArgumentException("Invalid operation! Allowed operations are +,-,*,/");
		}
	}

	public double tos() {
		if(list.isEmpty()){
			throw new RuntimeException("stack is empty!");
		}
		else 
		return list.peek(); //vaatab, aga ei võta
	}

	@Override
	public boolean equals(Object o) {

		DoubleStack tmp = (DoubleStack) o;

		return list.equals(tmp.list);
	}

	@Override
	public String toString() {

		StringBuffer b = new StringBuffer();
		for (int i = list.size() - 1; i > -1; i--) {
			b.append(String.valueOf(list.get(i)) + " ");

		}
		return b.toString(); // TODO!!! Your code here!
	}

	public static double interpret(String pol) {

		String[] exp = pol.trim().split("\\s+");// eemaldame algusest ja lõpust
												// tühikud ja splitime tühikute
												// järgi massiivi
		DoubleStack lt = new DoubleStack();// magasin, kus hakkame toimetama
		int n = 0;//operandide loendur
		int o = 0;//operaatorite loendur
		for (int i = 0; i < exp.length; i++) {
			if (exp[i].equals("/") || exp[i].equals("*") || exp[i].equals("+") || exp[i].equals("-")) {
				try {
					lt.op(exp[i]);
					o++;
				} catch (Exception e) {
					throw new RuntimeException("Not enough operands in expression! Expression \"" + pol +"\"");
				}
			} else {
				try {
					double d = Double.valueOf((exp[i]));
					lt.push(d);
					n++;
				} catch (Exception e) {
					throw new RuntimeException("Cannot convert argument \"" + exp[i] + "\" to double in expression " + pol);
				}
			}

		}
		if((n - 1) != o){
			throw new RuntimeException("Stack is not balanced. Operands :" + n + " Operators :" + o);
		}

		return lt.pop(); // TODO!!! Your code here!
	}

}
